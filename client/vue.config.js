module.exports = {
    devServer: {
        port: 9000,
        proxy: 'http://localhost:8080/api'
    },
    outputDir: '../src/main/resources/static/'
}